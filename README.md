# TIN BitTorrent

Projekt obsługujący przesyłanie plików P2P przy pomocy sieci BitTorrent.

## Zależności
```
pip3 install -r requirements.txt
```
## Kompilacja
```
cd tin-bittorrent
make
```
## Uruchomienie
### Serwera
```
./bin/server
```
Zostanie wypisany port na którym serwer nasłuchuje.

W każdym momencie można wyłączyć serwer wpisując Q lub q.
### Klienta
```
python3 main.py
```
