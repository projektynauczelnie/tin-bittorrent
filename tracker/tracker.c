#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>


#include "tracker.h"

#define BUFFER_SIZE 512
#define MESSAGE_SIZE 16
#define PORT_NUMBER_RECEIVING_SIGNALS 3491

// struktura do przechowywania i przekazywania informacji o porcie
struct port
{
	int sock;
	int length;
	struct sockaddr_in si_me;
};

struct seeder
{
	uint32_t ip;
	uint16_t port;
};

struct port open_new_port(int port_number)
{
	struct sockaddr_in si_me;
	int sock, length;

	if ((sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	{
		perror("socket");
	}

	memset((char *) &si_me, 0, sizeof(si_me));

	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(port_number);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
	
	if( bind(sock , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
	{
		perror("bind");
	}
	length = sizeof(si_me);
	struct port p1;
	p1.length = length;
	p1.si_me = si_me;
	p1.sock = sock;
	return p1;
}

uint32_t atoi2(unsigned char* act, int size)
{
	uint32_t number = 0;
	for( int i = 0; i < size; ++i )
	{
		number *= 256;
		number += act[i];
	}
	return number;
}

// odeslij do klienta akceptacje polaczenia
void accept_connection(unsigned char* buf, int sock, struct sockaddr_in si_other, int slen)
{
	// zmiana kolejności
	// action_id, transaction_id, connection_id
	unsigned char bufToSend[MESSAGE_SIZE];
	memcpy(bufToSend + 8, buf, 8);
	memcpy(bufToSend, buf+8, 8);


	if (sendto(sock, bufToSend, MESSAGE_SIZE, 0, (struct sockaddr*) &si_other, slen) == -1)
		perror("sending datagram message");

}

// dodawanie numeru do bufora
void add_number_to_buffor(unsigned char* bufToSend, uint32_t number, int offser)
{
	while( number > 0 )
	{
		unsigned char x = number%256;
		number /= 256;
		memcpy(bufToSend + offser, &x, 1);
		--offser;
	}
}

void add_seeder(unsigned char* bufToSend, int offset, uint32_t ip, uint16_t port)
{
	memcpy(bufToSend + offset, &ip, 4);
	add_number_to_buffor(bufToSend, port, offset + 5);
}

uint32_t add_seeders(char* fileName, unsigned char* bufToSend)
{
	FILE *fp;
	char buff[21];
	char* line = NULL;
	size_t len = 0;
	int offset = 20;
	int seeders = 0;

	fp = fopen("tracker/tracker_db.txt", "r");

	getline(&line, &len, fp);
	while(strlen(line) > 0)
	{
		char* pch;
		uint32_t ip = 0;
		uint16_t port = 0;

		pch = strtok (line," ");
		sscanf(pch, "%s", buff);
		pch = strtok (NULL, " ");
		if( strncmp(buff, fileName,sizeof(fileName)) == 0 )
		{
			while (pch != NULL && offset + (seeders + 1) * 6 <= BUFFER_SIZE )
			{
				sscanf(pch, "%d", &ip);
				pch = strtok (NULL, " ");
				sscanf(pch, "%hd", &port);
				pch = strtok (NULL, " ");
				add_seeder(bufToSend, offset + seeders * 6, ip, port);
				++seeders;
				ip = 0;
				port = 0;
			}
		}
		len = 0;
		line = NULL;
		getline(&line, &len, fp);
	}
	return seeders;
}

void add_seeder_to_list(char* fileName, struct sockaddr_in* si)
{
	uint32_t si_ip = si->sin_addr.s_addr;
	uint16_t si_port = si->sin_port;
	FILE *input_file;
	FILE *output_file;
	char buff[21];
	char* line = NULL;
	size_t len = 0;
	int is_added = 0;
	int exists = 0;

	input_file = fopen("tracker/tracker_db.txt", "r");
	output_file = fopen("tracker/tracker_db1.txt", "w+");

	getline(&line, &len, input_file);
	while(strlen(line) > 0)
	{
		char* pch;
		uint32_t ip = 0;
		uint32_t port = 0;

		pch = strtok (line," ");
		sscanf(pch, "%s", buff);
		pch = strtok (NULL, " ");
		fprintf(output_file,"%s", buff);
		while (pch != NULL)
		{
			sscanf(pch, "%d", &ip);
			pch = strtok (NULL, " ");
			sscanf(pch, "%d", &port);
			pch = strtok (NULL, " ");
			fprintf(output_file," %d %d", ip, port);
			if( strncmp(buff, fileName,sizeof(fileName)) == 0)
			if (port == si_port && ip == si_ip)
			{
				exists = 1;
				is_added = 1;
			}
			ip = 0;
			port = 0;
		}
		if( strncmp(buff, fileName,sizeof(fileName)) == 0 && exists == 0)
		{
			is_added = 1;
			fprintf(output_file," %d %d", si_ip, si_port);
		}
		len = 0;
		line = NULL;
		getline(&line, &len, input_file);
		if(strlen(line) > 0)
			fputc('\n',output_file);
	}
	fclose(input_file);
	fclose(output_file); 
	remove("tracker/tracker_db.txt"); 
	rename("tracker/tracker_db1.txt", "tracker/tracker_db.txt");
	if(is_added == 0)
		add_file_to_list(fileName, si);
}

void add_file_to_list(char* fileName, struct sockaddr_in* si)
{
	uint32_t si_ip = si->sin_addr.s_addr;
	uint32_t si_port = si->sin_port;
	FILE *output_file;

	output_file = fopen("tracker/tracker_db.txt", "a");

	fprintf(output_file,"\n%s %d %d", fileName, si_ip, si_port);

	fclose(output_file);
}

int check_file_owner(char* fileName, struct sockaddr_in* si)
{
	uint32_t si_ip = si->sin_addr.s_addr;
	uint16_t si_port = htons(si->sin_port);

	FILE *input_file;

	char* line = NULL;
	size_t len = 0;
	char buff[21];

	input_file = fopen("tracker/tracker_db.txt", "r");

	char* pch;
	uint32_t ip = 0;
	uint16_t port = 0;

	do
	{
		getline(&line, &len, input_file);
		pch = strtok (line," ");
		sscanf(pch, "%s", buff);
		pch = strtok (NULL, " ");
		printf("%d %s %d %s\n",sizeof(buff), buff,sizeof(fileName), fileName);
		//zostawiamy bez zmiany
		if( strncmp(buff, fileName,sizeof(fileName)) == 0 )
		{
			printf("ta sama nazwa\n");
			sscanf(pch, "%d", &ip);
			pch = strtok (NULL, " ");
			sscanf(pch, "%hd", &port);
			pch = strtok (NULL, " ");
			printf("comparing %d %d, %hd %hd",ip, si_ip, port, si_port);
			if(ip == si_ip && port == si_port)
			{
				fclose(input_file);
				return 1;
			}
			else{
				fclose(input_file);
				return 0;
			}
		}
	}while(strlen(line) > 0);
}


void remove_file(char* fileName, struct sockaddr_in* si, int sock, char* bufBase)
{
	uint32_t si_ip = si->sin_addr.s_addr;
	uint16_t si_port = htons(si->sin_port);
	FILE *input_file;
	FILE *output_file;
	char buff[21];
	char* line = NULL;
	size_t len = 0;
	int is_added = 0;
	printf("Revoke %s by %d:%d",fileName, si_ip, si_port);
	
	if(check_file_owner(fileName, si) == 1)
	{
		printf("is owner\n");
		input_file = fopen("tracker/tracker_db.txt", "r");
		output_file = fopen("tracker/tracker_db1.txt", "w+");

		char* pch;
		uint32_t ip = 0;
		in_port_t port = 0;
		
		getline(&line, &len, input_file);
		pch = strtok (line," ");
		sscanf(pch, "%s", buff);
		pch = strtok (NULL, " ");
		while(strlen(line) > 0)
		{
			//zostawiamy bez zmiany
			if( strncmp(buff, fileName,sizeof(fileName)) != 0 )
			{
				fprintf(output_file,"%s", buff);
				while (pch != NULL)
				{
					sscanf(pch, "%d", &ip);
					pch = strtok (NULL, " ");
					sscanf(pch, "%hd", &port);
					pch = strtok (NULL, " ");
					fprintf(output_file," %d %hd", ip, port);
					ip = 0;
					port = 0;
				}
				len = 0;
				line = NULL;
				getline(&line, &len, input_file);
				pch = strtok (line," ");
				sscanf(pch, "%s", buff);
				pch = strtok (NULL, " ");
				if(strlen(line) > 0 && strncmp(buff, fileName,sizeof(fileName)) != 0 )
					fprintf(output_file,"\n");
			}
			// usuwamy
			else
			{
				while (pch != NULL)
				{
					printf("scanning");
					sscanf(pch, "%d", &ip);
					pch = strtok (NULL, " ");
					sscanf(pch, "%hd", &port);
					pch = strtok (NULL, " ");

					struct sockaddr_in si_client;

					si_client.sin_family = AF_INET;
					si_client.sin_port = htons(port);
					si_client.sin_addr.s_addr = htonl(ip);
					
					// if( bind(sock , (struct sockaddr*)&si_client, sizeof(si_client) ) == -1)
					// {
					// 	perror("bind");
					// }
					printf("sending\n");
					char bufToSend[BUFFER_SIZE];
					strcpy(bufToSend, bufBase);
					strcpy(bufToSend + 16, fileName);
					if (sendto(sock, bufToSend, 36, 0, (struct sockaddr*) &si_client, sizeof(si_client)) == -1)
						perror("Revoke sending datagram message");
					printf("sent\n");
					ip = 0;
					port = 0;
				}
				printf("out of while");
				len = 0;
				line = NULL;
				getline(&line, &len, input_file);
				if(strlen(line) > 0)
				{
					pch = strtok (line," ");
					sscanf(pch, "%s", buff);
					pch = strtok (NULL, " ");
					if(strncmp(buff, fileName,sizeof(fileName)) != 0 )
						fprintf(output_file,"\n");
				}
			}

			
		}
		fclose(input_file);
		fclose(output_file);
		remove("tracker/tracker_db.txt"); 
		rename("tracker/tracker_db1.txt", "tracker/tracker_db.txt");
	}
}

void correct_remove_file(char* fileName, struct sockaddr_in* si, int sock, char* bufBase)
{
	uint32_t si_ip = si->sin_addr.s_addr;
	uint16_t si_port = htons(si->sin_port);
	FILE *input_file;
	FILE *output_file;
	char buff[21];
	char *line = NULL;
	size_t len = 0;
	int is_added = 0;
	char *pch;
	uint32_t ip = 0;
	in_port_t port = 0;
	printf("Revoke %s by %d:%d", fileName, si_ip, si_port);
	input_file = fopen("tracker/tracker_db.txt", "r");
	output_file = fopen("tracker/tracker_db1.txt", "w+");
	getline(&line, &len, input_file);
	do 
	{
	// pch = strtok(line, " ");
	sscanf(line, "%s %d %hd", &buff, &ip, &port);
	if( strncmp(buff, fileName,sizeof(fileName)) != 0 ){
		puts("WSZEDŁEM DO IF, NAZWY SIĘ NIE ZGADZAJĄ");
		fprintf(output_file, "%s", line);
	}
	else{
		puts("WSZEDŁEM DO ELSE");
		if(ip == si_ip && port == si_port)
			{
				puts("IP I PORTY SIE ZGADZAJĄ");
				pch = strtok(line, " ");
				pch = strtok(NULL, " ");
				pch = strtok(NULL, " ");
				pch = strtok(NULL, " ");
				while (pch != NULL)
				{
					puts("POCZĄTEK WHILE");
					printf("scanning");
					sscanf(pch, "%d", &ip);
					pch = strtok(NULL, " ");
					sscanf(pch, "%hd", &port);
					pch = strtok(NULL, " ");

					struct sockaddr_in si_client;
					memset((char *)&si_client, 0, sizeof(si_client));

					si_client.sin_family = AF_INET;
					si_client.sin_port = ntohs(port);
					si_client.sin_addr.s_addr = ip;

					// if( bind(sock , (struct sockaddr*)&si_client, sizeof(si_client) ) == -1)
					// {
					// 	perror("bind");
					// }
					printf("sending\n");
					char bufToSend[BUFFER_SIZE];
					memcpy(bufToSend, bufBase, 16);
					//strcpy(bufToSend, bufBase);
					//strcpy(bufToSend + 16, fileName);
					memcpy(bufToSend+16,fileName,20);
					int action = atoi2(bufToSend, 4);
					printf("ACTION %d: \n",action);
					printf("Bufbase i filename: %s %s\n",bufBase, fileName);
					int action2=atoi2(bufBase,4);
					printf("BUFBASE ACTION %d:\n",action2);
					printf("PORT:%d\n",si_client.sin_port);
					printf("IP: %d\n",si_client.sin_addr.s_addr);
					printf("SENDING A MESSAGE %s to port %d address %d\n",bufToSend,port,ip);
					if (sendto(sock, bufToSend, 36, 0, (struct sockaddr *)&si_client, sizeof(si_client)) == -1)
						perror("Revoke sending datagram message");
					printf("sent\n");
					ip = 0;
					port = 0;
				}
			}
		}
		line = NULL;
		getline(&line, &len, input_file);
		// może te 2 niżej niepotrzebne
		ip = 0;
		port = 0;

	}
	while (strlen(line) > 0);
	fclose(input_file);
	fclose(output_file);
	remove("tracker/tracker_db.txt");
	rename("tracker/tracker_db1.txt", "tracker/tracker_db.txt");
}

// nasłuchiwanie sygnałów od klientów
void *listener_function(void *vargp)
{
	unsigned char buf[BUFFER_SIZE];

	struct sockaddr_in si_me, si_other;
	int sock, slen = sizeof(si_other), recv_len, length;
	struct port *port = vargp;
	sock = port->sock;
	length = port->length;
	si_me = port->si_me;

	if (getsockname(sock, (struct sockaddr *)&si_me, &length) == -1)
	{
		perror("getting socket name");
		exit(1);
	}

	// nasłuchujemy
	while(1)
	{
		// otrzymanie wiadomości i odesłanie jej
		// connection_id, action_id, transaction_id
		if ((recv_len = recvfrom(sock, buf, 96, 0, (struct sockaddr *) &si_other, &slen)) == -1)
		{
			perror("recvfrom()");
		}

		// otrzymana wiadomosc
		// printf("-->");
		// for(int i = 0; i < MESSAGE_SIZE; ++i)
		// {
		// 	printf("%d ", buf[i]);
		// }
		// printf(" <--\n");
		
		unsigned char act[4];
		memcpy(act, buf + 8, 4);
		int action = atoi2(act, 4);
		// potwierdzenie polaczenia
		if( action == 0 )
		{
			//printf("From %d:%d ",si_other.sin_addr,listener_port);
			printf("handshake\n");
			accept_connection(buf, sock, si_other, slen);
		}
		// wyslanie informacji o seederach
		else if( action == 1 )
		{
			unsigned char bufToSend[BUFFER_SIZE];
			// action_id i transaction_id na początek wiadomosci
			memcpy(bufToSend, buf+8, 8);
			
			// dodaj do bufora wysylkowego liczbe seederow
			int32_t seeders = 1;
		
			// wyciagnij nazwe pliku z bufora wejsciowego
			char fileName[21];
			memcpy(fileName, buf+16, 20);
			printf("From %d:%d ",si_other.sin_addr.s_addr,si_other.sin_port);
			printf("Requested seeders for %s\n", fileName);
			// znajdz seederow
			seeders = add_seeders(fileName, bufToSend);
			// dodaj ilu seederow jest w bufforze
			add_number_to_buffor(bufToSend, seeders, 19);

			// odeslanie wiadomosci o seederach 
			if (sendto(sock, bufToSend, 20 + 6 * seeders, 0, (struct sockaddr*) &si_other, slen) == -1)
				perror("sending datagram message");
		}
		// dodanie nowego seedera
		else if( action == 2 )
		{
			unsigned char bufToSend[BUFFER_SIZE];
			// action_id i transaction_id na początek wiadomosci
			memcpy(bufToSend, buf+8, 8);
		
			// wyciagnij nazwe pliku z bufora wejsciowego
			char fileName[21];
			memcpy(fileName, buf+16, 20);
			printf("From %d:%d ",si_other.sin_addr.s_addr,si_other.sin_port);
			printf("Adding new seeder for %s\n", fileName);
			// !!! specjalny port listenera do zapisu !!!
			int16_t  listener_port;
			uint16_t temp_port;
			temp_port = si_other.sin_port;
			memcpy(&listener_port, buf+36, 2);
			si_other.sin_port = htons(listener_port);

			// dodaj klienta do listy seederow
			add_seeder_to_list(fileName, &si_other);

			// przywroc stary port
			si_other.sin_port = temp_port;

			if (sendto(sock, bufToSend, 8, 0, (struct sockaddr*) &si_other, slen) == -1)
				perror("sending datagram message");
		}
		// usunięcie pliku
		else if( action == 3 )
		{
			unsigned char bufToSend[BUFFER_SIZE];
			// action_id i transaction_id na początek wiadomosci
			memcpy(bufToSend, buf+8, 8);
		
			// wyciagnij nazwe pliku z bufora wejsciowego
			char fileName[21];
			memcpy(fileName, buf+16, 20);
			printf("From %d:%d ",si_other.sin_addr.s_addr,si_other.sin_port);
			printf("Revoking %s\n", fileName);
			// dodaj klienta do listy seederow
			correct_remove_file(fileName, &si_other, sock, bufToSend);
		}
	}
    return NULL;
}

int main(void)
{
	char ch = 0;
	
	struct port p1 = open_new_port(PORT_NUMBER_RECEIVING_SIGNALS);
	
	pthread_t thread_id;
    printf("Q/q to shut down server\n");
    pthread_create(&thread_id, NULL, listener_function, &p1);
	
	while(ch != 'q' && ch != 'Q')
	{
		scanf("%c", &ch);
	}
	pthread_cancel(thread_id);

	close(p1.sock);
	exit(0);
}