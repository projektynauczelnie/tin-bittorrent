import argparse

from client.client import Client
from client.menu import Menu

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--ip", action="store", help="IP address of client", default="localhost"
    )
    parser.add_argument(
        "--port", action="store", help="IP port of client", type=int, default=1240
    )
    args = parser.parse_args()
    client = Client(args.ip, args.port)
    menu = Menu(client)
    while menu.get_input():
        pass
    client.join_threads()
    print("THE PROGRAM ENDED NORMALLY")
