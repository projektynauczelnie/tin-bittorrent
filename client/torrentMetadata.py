from torf import Torrent


class TorrentMetadata:
    def __init__(self, filename):
        try:
            t = Torrent.read(filename)
        except:
            raise Exception("No such torrent file exists")
        self.url_port = self.parse_announce(t.metainfo["announce"])
        info_dict = t.metainfo["info"]
        self.length = info_dict["length"]
        self.name = info_dict["name"]
        if len(self.name) > 20:
            raise Exception(
                "Torrent's name is too long, the maximum file name length is 20"
            )
        self.filename = self.name
        self.name = bytes(self.name.ljust(20, "\0"), "utf-8")
        self.piece_length = info_dict["piece length"]
        self.pieces = info_dict["pieces"]

    def parse_announce(self, announce) -> tuple((str, int)):
        parsed = announce.rstrip("/announce")
        port_idx = parsed.rfind(":")
        port = parsed[port_idx + 1:]
        url_idx = parsed.rfind("/")
        url = parsed[url_idx + 1: port_idx]
        return url, int(port)
