import logging
from os import path
import socket


from client.baseManager import BaseManager
import client.client_config as config
from client.constants import REQUEST_ID
from client.utils import pack_string, unpack_string, send_packet, open_socket


class UploadManager(BaseManager):
    def __init__(self, action, transaction_id, connection_id, file_name, addr):
        self.action = action
        self.transaction_id = transaction_id
        self.connection_id = connection_id
        self.file_name = file_name
        self.addr = addr  # the other client's address, not ours
        self.socket = open_socket()

    def execute(self):
        packet = pack_string(
            "!iiq", self.action, self.transaction_id, self.connection_id
        )
        self.socket.settimeout(5.0)  # higher timeout here than default
        try:
            response = send_packet(self.socket, self.addr, packet)
        except Exception as e:
            logging.error(str(e))
            return
        while 1:
            try:
                action, transaction_id, connection_id, index, length = unpack_string(
                    "!iiqii", response
                )
                if action != REQUEST_ID:
                    logging.error(
                        "What was expected was a request for some block in a file"
                    )
                    raise Exception
                if (
                    transaction_id != self.transaction_id
                    or connection_id != self.connection_id
                ):
                    logging.error(
                        "Wrong transaction or connection supplied with the request"
                    )
                    raise Exception
                place_in_file = index * length
                file_path = path.join(config.shared_folder, self.file_name)
                file_contents = None
                file_path = file_path.replace(
                    "\0", ""
                )  # there are some trailing zeros which need to be trimmed
                with open(file_path, "rb") as wf:
                    try:
                        file_contents = wf.read()
                    except Exception as e:
                        logging.error(
                            f"COULD NOT READ FILE DUE TO EXCEPTION: {str(e)}")
                        return
                if file_contents is None:
                    logging.error(
                        "The file is empty or could not even be read")
                    raise Exception
                block = file_contents[place_in_file: place_in_file + length]
                packet = response + block
                try:
                    response = send_packet(self.socket, self.addr, packet)
                except socket.timeout:
                    return
            except Exception as e:
                logging.error(str(e))
                return

    def __repr__(self):
        return "UploadManager: file="
