MESSAGE_SIZE = 512  # standard message size
BUFFER_SIZE = 1024  # standard buffer size
CONNECT_ID: int = 0  # used for handshakes
DOWNLOAD_ID: int = 1  # used for downloading peer list
ANNOUNCE_ID: int = 2  # used for announcing that we have a new file
REVOKE_ID: int = 3  # used for revoking certain torrent
UPLOAD_ID: int = 4  # used when peer wants us to send something (or, analogically, when we want to download something)
REQUEST_ID: int = 6  # used when we want to request a piece of some file, it's 6 not 5 to comply with standard
