import hashlib
import logging
import random
import socket
from os import path
from pathlib import Path

import client.client_config as config
from client.baseManager import BaseManager
from client.constants import DOWNLOAD_ID, UPLOAD_ID, REQUEST_ID, CONNECT_ID, ANNOUNCE_ID
from client.torrentMetadata import TorrentMetadata
from client.utils import (
    send_packet,
    pack_string,
    unpack_string,
    send_packet_adress,
)


class DownloadManager(BaseManager):
    def __init__(self, filename: str):
        super().__init__()
        self.filename = filename
        self.key = int(random.getrandbits(31))
        self.upload_address = None
        self.file_data = bytes("", encoding="utf-8")

    def execute(self):
        try:
            self.metainfo = TorrentMetadata(self.filename)
        except Exception as e:
            logging.error(
                f"Provided .torrent file does not exist {self.filename}")

        self.tracker_address = self.metainfo.url_port
        logging.info(f"UWAGA {self.connection_id}")
        logging.info(f"TRACKER ADDRESS: {self.tracker_address}")

        self.parse_response(self.connect_to_tracker(), CONNECT_ID)
        get_peer_list_response = self.parse_response(
            self.announce(), DOWNLOAD_ID)
        peer_list = self.get_peer_list(get_peer_list_response)
        Path(config.shared_folder).mkdir(exist_ok=True)
        # we try every peer until we succeed
        for ip, port in peer_list:
            try:
                response, self.upload_address = self.request_download(
                    ip, port, self.metainfo.name
                )
                response = self.parse_response(response, UPLOAD_ID)
            except Exception as e:
                logging.warning(f"{ip}:{port}-{str(e)}")
                continue
            if self.upload_address:  # that means we actually connected to someone
                has_crashed = False
                logging.info(
                    f"Rozpoczynam pobieranie pliku {self.metainfo.filename} od: {ip}:{port}"
                )
                # ask for each piece separately
                for index, piece_hash in enumerate(self.chunks(self.metainfo.pieces)):
                    logging.info(f"INDEX: {index} HASH: {piece_hash}")
                    try:
                        response = self.request_packet(
                            self.upload_address[0], self.upload_address[1], index
                        )
                        response = self.parse_response(response, REQUEST_ID)
                        chunk = response[24:]
                        self.check_hash(chunk, piece_hash)
                        self.file_data += chunk
                        logging.info(f"ROZMIAR: {len(chunk)}")
                    except Exception as e:
                        logging.error(str(e))
                        has_crashed = True
                        break
                # that means we had some time and we downloaded it successfully
                if self.file_data and not has_crashed:
                    logging.info(f"ROZMIAR FILE DATA: {len(self.file_data)}")
                    file_path = path.join(
                        config.shared_folder, self.metainfo.filename)
                    with open(file_path, "wb") as file:
                        file.write(self.file_data)
                        self.tell_tracker_about_file()
                        return
                else:
                    logging.error("Unable to download file from other clients")

    def check_hash(self, chunk, piece_hash):
        sha1 = hashlib.sha1()
        sha1.update(chunk)
        if sha1.digest() != piece_hash:
            logging.error("hash do not match")
            raise Exception

    def chunks(self, lst):
        for i in range(0, len(lst), 20):
            yield lst[i: i + 20]

    def request_packet(self, ip, port, index):
        message = pack_string(
            "!iiqii",
            REQUEST_ID,
            self.current_transaction_id,
            self.connection_id,
            index,
            self.metainfo.piece_length,
        )
        return send_packet(
            self.socket, (ip, port), message, self.metainfo.piece_length + 24
        )

    def request_download(self, ip, port, filename):
        message = self.upload_message(UPLOAD_ID, filename)
        return send_packet_adress(self.socket, (ip, port), message)

    def __repr__(self):
        return "DownloadManager: file="

    def upload_message(self, action, filename):
        return pack_string(
            "!iiq", action, self.current_transaction_id, self.connection_id
        ) + bytes(filename)

    def announce_message(self, file_length, info_hash):
        self.current_transaction_id = int(random.getrandbits(31))
        downloaded_bytes = 0
        bytes_left = file_length - downloaded_bytes
        uploaded_bytes = 0
        event = 0
        ip = 0
        num_want = -1
        download_info = pack_string(
            "!qqqiiiih",
            downloaded_bytes,
            bytes_left,
            uploaded_bytes,
            event,
            ip,
            self.key,
            num_want,
            3491,
        )
        return info_hash + config.client_id + download_info

    def announce(self):
        announce_message = self.announce_message(
            self.metainfo.length, self.metainfo.name
        )
        pre_message = self.connection_message(DOWNLOAD_ID)
        announce_message = pre_message + announce_message
        response = send_packet(
            self.socket, self.tracker_address, announce_message)
        return response

    def get_peer_list(self, response):
        if len(response) < 20:
            logging.error("ERROR GETTING PEERS")
            raise Exception

        num_peers = unpack_string("!i", response[16:20])
        peers = []
        for n in range(len(num_peers)):
            peer_start = 20 + 6 * n  # values due to standard
            peer_end = peer_start + 4
            ip = socket.inet_ntoa(response[peer_start:peer_end])
            port = unpack_string("!H", response[peer_end: peer_end + 2])[0]
            peers.append((ip, port))
        return peers

    def tell_tracker_about_file(self):
        port = config.listener_port.to_bytes(2, byteorder="big")
        announce_message = (
            self.connection_message(ANNOUNCE_ID) + self.metainfo.name + port
        )
        response = send_packet(
            self.socket, self.tracker_address, announce_message)
        self.parse_response(response, ANNOUNCE_ID)
        return response
