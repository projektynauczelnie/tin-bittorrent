import logging
import os
import threading
from client.listener import Listener
import client.client_config as config


class Client:
    def __init__(self, ip: str, port: int):
        self.threads = []
        self.managers = []
        config.client_id = f"TINTORRENT-{port}".ljust(20, "0")[
            :20].encode("utf-8")
        self.ip = ip
        self.port = port
        config.shared_folder = "shared_" + str(port)
        config.listener_IP = ip
        config.listener_port = port
        os.makedirs(config.shared_folder, exist_ok=True)
        logging.basicConfig(
            filename=f"{config.shared_folder}/client.log", level=logging.DEBUG
        )
        self.listener = self.create_listener()

    def join_threads(self):
        for thread in self.threads:
            thread.join()

    def create_listener(self):
        listener = Listener(self.ip, self.port)
        thrd = threading.Thread(target=listener.listen)
        self.threads.append(thrd)
        thrd.start()
        return listener
