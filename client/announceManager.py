import logging
from pathlib import Path
import random
import shutil

from torf import Torrent

from client.baseManager import BaseManager
import client.client_config as config
from client.constants import CONNECT_ID, ANNOUNCE_ID
from client.utils import send_packet


class AnnounceManager(BaseManager):
    def __init__(self, listener_port, tracker_name, tracker_address, file):
        super().__init__()
        self.tracker_name = tracker_name
        self.tracker_address = tracker_address
        self.listener_port = listener_port
        self.file = file
        self.file_name = Path(self.file).name
        if len(self.file_name) > 20:
            logging.error("NAME CANNOT BE LONGER THAN 20 CHARACTERS")
            raise ValueError

    def execute(self):
        Path("torrent_files").mkdir(exist_ok=True)
        torr_name = Path(self.file).stem
        try:
            t = Torrent(path=self.file, trackers=[self.tracker_name])
        except Exception as e:
            logging.error(f"No such file to upload {self.file_name}")
        t.generate()
        t.write(f"torrent_files/{torr_name}.torrent",
                validate=True, overwrite=True)
        shutil.copy(self.file, config.shared_folder)
        self.send_file()

    def announce(self):
        port = self.listener_port.to_bytes(2, byteorder="big")
        announce_message = (
            self.connection_message(ANNOUNCE_ID)
            + bytes(self.file_name.ljust(20, "\0"), "utf-8")
            + port
        )
        response = send_packet(
            self.socket, self.tracker_address, announce_message)
        return response

    def send_file(self):
        self.current_transaction_id = int(random.getrandbits(31))
        self.parse_response(self.connect_to_tracker(), CONNECT_ID)
        self.parse_response(self.announce(), ANNOUNCE_ID)

    def __repr__(self):
        return "AnnounceManager: file="
