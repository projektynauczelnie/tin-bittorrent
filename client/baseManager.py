import random

from client.utils import pack_string, unpack_string, open_socket, send_packet
from client.constants import CONNECT_ID


class BaseManager:
    def __init__(self):
        self.current_transaction_id = int(random.getrandbits(31))
        self.connection_id = int("41727101980", 16)
        self.socket = 0

    def execute(self):
        return

    def __repr__(self):
        return "BaseManager:"

    def connection_message(self, action):
        return pack_string(
            "!qii", self.connection_id, action, self.current_transaction_id
        )

    def parse_response(self, response, expected_action):
        action = unpack_string("!i", response[:4])[0]
        if action != expected_action:
            raise Exception("ACTION ERROR!")
        transaction_id = unpack_string("!i", response[4:8])[0]
        if transaction_id != self.current_transaction_id:
            raise Exception("TRANSACTIONS ID DO NOT MATCH!")
        connection_id = 0
        if action == CONNECT_ID:
            connection_id = unpack_string("!q", response[8:16])[0]
            self.connection_id = connection_id
        return response

    def connect_to_tracker(self):
        self.socket = open_socket()
        message = self.connection_message(CONNECT_ID)
        response = send_packet(self.socket, self.tracker_address, message)
        return response
