import os
import threading

from client.announceManager import AnnounceManager
from client.downloadManager import DownloadManager
from client.revokeManager import RevokeManager


def screen_clear():
    # for mac and linux(here, os.name is 'posix')
    if os.name == "posix":
        _ = os.system("clear")
    else:
        # for windows platfrom
        _ = os.system("cls")


class Menu:
    def __init__(self, client):
        self.main_menu = {
            1: ("Upload file", self.upload),
            2: ("Download file", self.download),
            3: ("Revoke upload", self.revoke),
            4: ("QUIT", self.quit),
        }
        self.client = client

    def get_files(self, messages=None):
        if messages is None:
            messages = ["", ""]
        screen_clear()
        inp = input(
            f"ENTER PATH TO{messages[0]} file you want to {messages[1]} (for multiple separate with SPACE): "
        )
        if not (files := inp.split()):
            print("ERROR NO FILES PROVIDED!")
            return None
        print(files)
        return files

    def upload(self):
        if (files := self.get_files(["", "upload"])) is None:
            return True
        inp = input("ENTER TRACKER IP (default localhost): ")
        tracker_ip = inp if inp else "127.0.0.1"
        inp = input("ENTER TRACKER PORT (default 3491): ")
        tracker_port = None
        try:
            tracker_port = int(inp) if inp else 3491
        except ValueError:
            print("ERROR: TRACKER PORT MUST BE AN INTEGER")
            return True
        tracker = f"http://{tracker_ip}:{tracker_port}/announce"
        print(tracker)
        for file in files:
            manger = AnnounceManager(
                self.client.port, tracker, (tracker_ip, tracker_port), file
            )
            self.client.managers.append(manger)
            thrd = threading.Thread(target=manger.execute)
            self.client.threads.append(thrd)
            thrd.start()
        return True

    def download(self):
        if (files := self.get_files([" .torrent", "download"])) is None:
            return True
        for file in files:
            manger = DownloadManager(file)
            self.client.managers.append(manger)
            thrd = threading.Thread(target=manger.execute)
            self.client.threads.append(thrd)
            thrd.start()
        return True

    def revoke(self):
        if (files := self.get_files(["", "revoke"])) is None:
            return True
        for file in files:
            manger = RevokeManager(
                self.client.listener.socket, file, initiated=True)
            self.client.managers.append(manger)
            thrd = threading.Thread(target=manger.execute)
            self.client.threads.append(thrd)
            thrd.start()
        return True

    def quit(self):
        return False

    def print_menu(self, level):
        for key, value in level.items():
            print(key, value[0])

    def get_input(self, level=None):
        level = level if level else self.main_menu
        screen_clear()
        print("AVAILABLE OPTIONS")
        self.print_menu(level)
        inp = input("ENTER YOUR OPTION: ")
        try:
            inp = int(inp)
        except ValueError:
            return True

        if inp == 4:
            self.client.listener.finished = True
            print("LISTENER ENDED")

        if inp in level.keys():
            return level.get(inp)[1]()

        print("UNKNOWN OPTION")
        return True
