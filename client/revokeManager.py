import logging
import os
import random

from client.baseManager import BaseManager
import client.client_config as config
from client.constants import REVOKE_ID
from client.torrentMetadata import TorrentMetadata
from client.utils import pack_string


class RevokeManager(BaseManager):
    def __init__(self, socket, file, initiated=False):
        self.socket = socket
        self.file = file
        self.initiated = initiated

    def execute(self):

        try:
            if self.initiated:
                self.metainfo = TorrentMetadata(self.file)
                self.name = self.metainfo.filename
                self.tracker_address = self.metainfo.url_port
            else:
                self.name = self.file.rstrip("\x00")
        except Exception as e:
            logging.error(f"No such file to revoke: {self.file}")

        path = config.shared_folder + "/" + self.name
        logging.info(f"Removing {path}")
        try:
            os.remove(path)
        except OSError:
            logging.warning(
                f"There was no file named {self.name} in {config.shared_folder} directory"
            )

        if self.initiated:
            self.current_transaction_id = int(random.getrandbits(31))
            self.action = REVOKE_ID
            self.connection_id = int(random.getrandbits(63))
            packet = pack_string(
                "!qii", self.connection_id, self.action, self.current_transaction_id
            )
            packet = packet + self.metainfo.name
            self.socket.sendto(packet, self.tracker_address)

    def __repr__(self):
        return "RevokeManager: file="
