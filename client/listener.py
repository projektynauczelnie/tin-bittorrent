import threading
import socket
import logging

import client.client_config as config
from client.constants import MESSAGE_SIZE, ANNOUNCE_ID, CONNECT_ID, REVOKE_ID, UPLOAD_ID
from client.revokeManager import RevokeManager
from client.uploadManager import UploadManager
from client.utils import pack_string, unpack_string


class Listener:
    def __init__(self, ip, port):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.threads = []
        self.managers = []
        self.finished = False
        self.ip = ip
        self.port = port
        self.socket.bind((ip, port))
        self.socket.settimeout(2)

    def listen(self):
        logging.info("LISTENER STARTED")
        while not self.finished:
            try:
                data, addr = self.socket.recvfrom(MESSAGE_SIZE)
            except socket.timeout:
                continue
            logging.info(f"Received message from address: {addr[0]}:{addr[1]}")
            action = unpack_string("!i", data[:4])[0]
            if action == CONNECT_ID:
                logging.warning("Handshake in this place is unexpected")
            elif action == REVOKE_ID:
                # 4 bytes action, then 4 bytes transaction,8 bytes connection, 20 bytes for name
                file_name = str(data[16:36], encoding="utf-8")
                logging.info(f"Revoke: {file_name}")
                manager = RevokeManager(self.socket, file_name)
                self.managers.append(manager)
                thrd = threading.Thread(target=manager.execute)
                self.threads.append(thrd)
                thrd.start()

            elif action == UPLOAD_ID:
                # action 4, 4 bytes transaction, 8 bytes connection
                transaction_id, connection_id = unpack_string(
                    "!iq", data[4:16])
                file_name = str(data[16:36], encoding="utf-8")
                manager = UploadManager(
                    action, transaction_id, connection_id, file_name, addr
                )
                self.managers.append(manager)
                thrd = threading.Thread(target=manager.execute)
                self.threads.append(thrd)
                thrd.start()

            # it should not be there, but we can skip it for now
            elif action == ANNOUNCE_ID:
                continue

            else:
                logging.error(f"Unexpected action number: {action}")

        self.join_threads()

    def join_threads(self):
        for thread in self.threads:
            thread.join()
