import socket
import struct
import sys
from client.constants import MESSAGE_SIZE


def pack_string(struct_format, *args):
    try:
        s = struct.Struct(struct_format)
        packed = s.pack(*args)
        return packed
    except ValueError as e:
        print(e)


def unpack_string(struct_format, packed):
    try:
        s = struct.Struct(struct_format)
        unpacked = s.unpack(packed)
        return unpacked
    except ValueError as e:
        print(e)


def open_socket():
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(("", 0))
        sock.settimeout(2)
        return sock
    except socket.error:
        print("Could not create socket")
        sys.exit(1)


def send_packet(sock, address, packet, size=MESSAGE_SIZE):
    sock.sendto(packet, address)
    response = sock.recv(size)
    return response


def send_packet_adress(sock, address, packet):
    sock.sendto(packet, address)
    return sock.recvfrom(MESSAGE_SIZE)
