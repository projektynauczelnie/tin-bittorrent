CXX		  := gcc
CXX_FLAGS := -Wall -Wextra  -O2 


BIN		:= bin
LIB		:= lib
SERVER_DIR = tracker

LIBRARIES	:= -pthread


all: $(BIN)/tracker

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

$(BIN)/tracker: $(SERVER_DIR)/*.c
	mkdir -p $(BIN)
	$(CXX) $(CXX_FLAGS) -L $(LIB) $^ -o $@ $(LIBRARIES)

clean:
	-rm $(BIN)/*

